{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "60dfa57d-211e-4a49-a476-9db5b87a62fa",
   "metadata": {},
   "source": [
    "# Predict the Cost of Homes in California"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0a77100-78c4-411d-8ffe-ea4818be6686",
   "metadata": {},
   "source": [
    "## AWS Atlanta Summit - 2022\n",
    "### Kesha Williams"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c844d8eb-826e-4f3e-ba00-bbc45f55f99d",
   "metadata": {},
   "source": [
    "This notebook trains a machine learning model that predicts home costs in California using linear regression and [Scikit-learn](https://scikit-learn.org/stable/index.html). \n",
    "\n",
    "The overall objective is to predict the value of home prices using **8 feature variables and 1 target variable:** \n",
    "\n",
    "#### **Feature Variables** ####\n",
    "\n",
    "1. **MedInc:** median income in block group\n",
    "2. **HouseAge:** median house age in block group\n",
    "3. **AveRooms:** average number of rooms per household\n",
    "4. **AveBedrms:** average number of bedrooms per household\n",
    "5. **Population:** block group population\n",
    "6. **AveOccup:** average number of household members\n",
    "7. **Latitude:** block group latitude\n",
    "8. **Longitude:** block group longitude\n",
    "\n",
    "#### **Target Variable** ####\n",
    "\n",
    "**MedHouseVal:** The target variable is the **median house value** for California districts, expressed in hundreds of thousands of dollars `($100,000)`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39949515-ed73-4db2-b16b-9b323a22d29d",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "## Background"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9370f84-46fc-4ad5-a050-22cb7efb665c",
   "metadata": {},
   "source": [
    "## Scikit-learn\n",
    "\n",
    "Scikit-learn is a machine learning library for the Python programming language. It features various classification, regression and clustering learning algorithms. \n",
    "\n",
    "## Data\n",
    "\n",
    "The training data is one of the sample datasets provided with Scikit-learn.\n",
    "\n",
    "https://scikit-learn.org/stable/datasets/real_world.html#california-housing-dataset\n",
    "\n",
    "## Linear Regression\n",
    "\n",
    "In machine learning, the ability of a model to predict  continuous or real values based on a training dataset is called Regression.\n",
    "\n",
    "## Dependencies\n",
    "\n",
    "- pandas - To work with solid data-structures, n-dimensional matrices and perform exploratory data analysis.\n",
    "- matplotlib - To visualize data using 2D plots.\n",
    "- seaborn - To make 2D plots look pretty and readable.\n",
    "- scikit-learn - To create machine learning models easily and make predictions.\n",
    "- numpy - To work with arrays."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9218d4a-e465-4a6b-b044-8e0ff6dfb5d6",
   "metadata": {
    "tags": []
   },
   "source": [
    "**A few things to note:**\n",
    "\n",
    "- You'll need to install the various libraries before importing them ```(i.e. %pip install pandas)```\n",
    "- You may need to restart the kernel to use updated packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "54cd501d-7a9f-4e50-b9c4-7deec20b4947",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import seaborn as sns \n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "from sklearn import datasets\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.model_selection import train_test_split, cross_val_score"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66cf17c9-eb5b-41c6-a5d1-5f7d6d385f23",
   "metadata": {},
   "source": [
    "## Step 1 Load and Prepare the Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "0e330286-d839-4ffd-9067-a881a241fd97",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ".. _california_housing_dataset:\n",
      "\n",
      "California Housing dataset\n",
      "--------------------------\n",
      "\n",
      "**Data Set Characteristics:**\n",
      "\n",
      "    :Number of Instances: 20640\n",
      "\n",
      "    :Number of Attributes: 8 numeric, predictive attributes and the target\n",
      "\n",
      "    :Attribute Information:\n",
      "        - MedInc        median income in block group\n",
      "        - HouseAge      median house age in block group\n",
      "        - AveRooms      average number of rooms per household\n",
      "        - AveBedrms     average number of bedrooms per household\n",
      "        - Population    block group population\n",
      "        - AveOccup      average number of household members\n",
      "        - Latitude      block group latitude\n",
      "        - Longitude     block group longitude\n",
      "\n",
      "    :Missing Attribute Values: None\n",
      "\n",
      "This dataset was obtained from the StatLib repository.\n",
      "https://www.dcc.fc.up.pt/~ltorgo/Regression/cal_housing.html\n",
      "\n",
      "The target variable is the median house value for California districts,\n",
      "expressed in hundreds of thousands of dollars ($100,000).\n",
      "\n",
      "This dataset was derived from the 1990 U.S. census, using one row per census\n",
      "block group. A block group is the smallest geographical unit for which the U.S.\n",
      "Census Bureau publishes sample data (a block group typically has a population\n",
      "of 600 to 3,000 people).\n",
      "\n",
      "An household is a group of people residing within a home. Since the average\n",
      "number of rooms and bedrooms in this dataset are provided per household, these\n",
      "columns may take surpinsingly large values for block groups with few households\n",
      "and many empty houses, such as vacation resorts.\n",
      "\n",
      "It can be downloaded/loaded using the\n",
      ":func:`sklearn.datasets.fetch_california_housing` function.\n",
      "\n",
      ".. topic:: References\n",
      "\n",
      "    - Pace, R. Kelley and Ronald Barry, Sparse Spatial Autoregressions,\n",
      "      Statistics and Probability Letters, 33 (1997) 291-297\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Import the California Housing dataset\n",
    "cali_homes = datasets.fetch_california_housing(as_frame=True)\n",
    "\n",
    "# Describe the dataset\n",
    "print(cali_homes.DESCR)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "b9dec34d-1b70-4802-afea-d6f2bd4288bd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>MedInc</th>\n",
       "      <th>HouseAge</th>\n",
       "      <th>AveRooms</th>\n",
       "      <th>AveBedrms</th>\n",
       "      <th>Population</th>\n",
       "      <th>AveOccup</th>\n",
       "      <th>Latitude</th>\n",
       "      <th>Longitude</th>\n",
       "      <th>PRICE</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>8.3252</td>\n",
       "      <td>41.0</td>\n",
       "      <td>6.984127</td>\n",
       "      <td>1.023810</td>\n",
       "      <td>322.0</td>\n",
       "      <td>2.555556</td>\n",
       "      <td>37.88</td>\n",
       "      <td>-122.23</td>\n",
       "      <td>4.526</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>8.3014</td>\n",
       "      <td>21.0</td>\n",
       "      <td>6.238137</td>\n",
       "      <td>0.971880</td>\n",
       "      <td>2401.0</td>\n",
       "      <td>2.109842</td>\n",
       "      <td>37.86</td>\n",
       "      <td>-122.22</td>\n",
       "      <td>3.585</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>7.2574</td>\n",
       "      <td>52.0</td>\n",
       "      <td>8.288136</td>\n",
       "      <td>1.073446</td>\n",
       "      <td>496.0</td>\n",
       "      <td>2.802260</td>\n",
       "      <td>37.85</td>\n",
       "      <td>-122.24</td>\n",
       "      <td>3.521</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>5.6431</td>\n",
       "      <td>52.0</td>\n",
       "      <td>5.817352</td>\n",
       "      <td>1.073059</td>\n",
       "      <td>558.0</td>\n",
       "      <td>2.547945</td>\n",
       "      <td>37.85</td>\n",
       "      <td>-122.25</td>\n",
       "      <td>3.413</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>3.8462</td>\n",
       "      <td>52.0</td>\n",
       "      <td>6.281853</td>\n",
       "      <td>1.081081</td>\n",
       "      <td>565.0</td>\n",
       "      <td>2.181467</td>\n",
       "      <td>37.85</td>\n",
       "      <td>-122.25</td>\n",
       "      <td>3.422</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   MedInc  HouseAge  AveRooms  AveBedrms  Population  AveOccup  Latitude  \\\n",
       "0  8.3252      41.0  6.984127   1.023810       322.0  2.555556     37.88   \n",
       "1  8.3014      21.0  6.238137   0.971880      2401.0  2.109842     37.86   \n",
       "2  7.2574      52.0  8.288136   1.073446       496.0  2.802260     37.85   \n",
       "3  5.6431      52.0  5.817352   1.073059       558.0  2.547945     37.85   \n",
       "4  3.8462      52.0  6.281853   1.081081       565.0  2.181467     37.85   \n",
       "\n",
       "   Longitude  PRICE  \n",
       "0    -122.23  4.526  \n",
       "1    -122.22  3.585  \n",
       "2    -122.24  3.521  \n",
       "3    -122.25  3.413  \n",
       "4    -122.25  3.422  "
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Convert data to a Pandas Dataframe\n",
    "cali_df = pd.DataFrame(cali_homes.data, columns = cali_homes.feature_names)\n",
    "\n",
    "# Add the target variable to the dataframe \n",
    "cali_df['PRICE'] = cali_homes.target\n",
    "\n",
    "# Print the first few rows\n",
    "cali_df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "03b8cdf0-92fa-4f66-aee4-cca45e1960d4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(20640, 9)"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# see the size of our dataset\n",
    "cali_df.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "82def54c-f77a-4ec6-bb30-4297f6d86c96",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "MedInc        0\n",
       "HouseAge      0\n",
       "AveRooms      0\n",
       "AveBedrms     0\n",
       "Population    0\n",
       "AveOccup      0\n",
       "Latitude      0\n",
       "Longitude     0\n",
       "PRICE         0\n",
       "dtype: int64"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Verify there are no missing values as these could skew the model\n",
    "cali_df.isnull().sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b83e0fa-e643-4bf6-bbd8-cdf392a7a136",
   "metadata": {},
   "source": [
    "### Use visualizations to understand the relationship of the target variable with other features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "03f4aa6b-d2cb-448e-8d2e-ad2fead1dfdd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAsAAAAH0CAYAAAAzLJvJAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAp1ElEQVR4nO3de3TfdWH/8VeTNr3RGlrbkrZoFbF2cgAhG8KcjvRogfWCbJNaBQ/XOURxA0bHurZcRNNydZSBw3lkQ1AUrSlgQauboDLKRSxFwHKRQWkhaUfpnSS/PzjmZ6WXQL9p0r4fj3M4h3zf38/7+04+zTfPfvrJ59Orvb29PQAAUIiq7l4AAADsSgIYAICiCGAAAIoigAEAKIoABgCgKAIYAICiCGAAAIrSu7sX8GatWrU2bW07voTx0KF7pbn5lV2wIrqT/VwO+7oM9nM57OsydMd+rqrqlb33HrjVsd02gNva2jsVwL97Lns++7kc9nUZ7Ody2Ndl6En72SkQAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFCU3t29AGDXGTS4f/r1rdy3/YaNr2bNy+srNh8A7AoCGArSr2/vTDp7fsXma7psStZUbDYA2DWcAgEAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARend3QsAtm/Q4P7p19e3KgBUip+q0MP169s7k86eX5G5mi6bUpF5AGB35hQIAACKIoABACiKAAYAoCidCuDGxsY0NDRk7Nixefzxx183fvXVV79u7KGHHsrkyZMzYcKEnHzyyWlubu7UGAAAdKVOBfD48eNz4403ZtSoUa8be+SRR/LQQw9tMdbW1pZzzz03M2fOzMKFC1NfX59LL710h2MAANDVOhXA9fX1qaure93jmzZtyoUXXpjZs2dv8fiSJUvSt2/f1NfXJ0mmTp2aH/zgBzscAwCArrZT5wBfddVVmTx5ckaPHr3F48uXL8/IkSM7Ph4yZEja2tqyevXq7Y4BAEBXe9PXAX7wwQezZMmSnHPOOZVcT6cNHbpXp587bNigLlwJPYX93D264+tuX5fBfi6HfV2GnrSf33QA33fffVm2bFnGjx+fJHnhhRdyyimn5Itf/GLq6ury/PPPdzy3paUlVVVVqa2t3e7YG9Hc/Era2tp3+LxhwwblxRfXvKG52f3syfu5J71hbM2u/rrvyfua/89+Lod9XYbu2M9VVb22ecD0TQfw6aefntNPP73j44aGhlx77bV597vfnba2tmzYsCGLFy9OfX19br755hx11FFJkgMOOGCbYwAA0NU6FcAXX3xx7rzzzrz00ks56aSTUltbm9tuu22bz6+qqsqcOXMya9asbNy4MaNGjcrcuXN3OAYAAF2tUwE8Y8aMzJgxY7vPWbRo0RYfH3LIIWlqatrqc7c3BgAAXcmd4AAAKIoABgCgKAIYAICivOmrQABbN2hw//Tr61sLAHoqP6Whwvr17Z1JZ8+v2HxNl02p2FwAgFMgAAAojAAGAKAoAhgAgKI4B5hdopK/GLZh46tZ8/L6iswFAJRHALNLVPIXw5oum5I1FZmJnbVpc2uGDRtUsfn85QaAXUEAA29aTZ/qil/xwl9uAOhqzgEGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKL07u4FQHcbNLh/+vX1rQAApfBTn+L169s7k86eX7H5mi6bUrG5AIDKcwoEAABFEcAAABRFAAMAUBQBDABAUQQwAABFEcAAABRFAAMAUBQBDABAUQQwAABFEcAAABRFAAMAUBQBDABAUXp39wLgjdq0uTXDhg163eNbewwA4A8JYHY7NX2qM+ns+RWbr+myKRWbCwDo+ZwCAQBAUQQwAABFEcAAABRFAAMAUBQBDABAUQQwAABFEcAAABSlUwHc2NiYhoaGjB07No8//niSZNWqVTnttNMyYcKETJo0KWeeeWZaWlo6tnnooYcyefLkTJgwISeffHKam5s7NQYAAF2pUwE8fvz43HjjjRk1alTHY7169cqpp56ahQsXpqmpKfvuu28uvfTSJElbW1vOPffczJw5MwsXLkx9fX2nxgAAoKt1KoDr6+tTV1e3xWO1tbU57LDDOj4++OCD8/zzzydJlixZkr59+6a+vj5JMnXq1PzgBz/Y4RgAAHS1ipwD3NbWlptuuikNDQ1JkuXLl2fkyJEd40OGDElbW1tWr1693TEAAOhqvSsxyUUXXZQBAwbkk5/8ZCWm65ShQ/fq9HOHDRvUhSsBKqkz36++p8tgP5fDvi5DT9rPOx3AjY2NeeaZZ3Lttdemquq1A8p1dXUdp0MkSUtLS6qqqlJbW7vdsTeiufmVtLW17/B5w4YNyosvrnlDc1N5PekPPT3bjr5ffU+XwX4uh31dhu7Yz1VVvbZ5wHSnToG4/PLLs2TJksybNy81NTUdjx9wwAHZsGFDFi9enCS5+eabc9RRR+1wDAAAulqnjgBffPHFufPOO/PSSy/lpJNOSm1tba688spcd911GTNmTKZOnZokGT16dObNm5eqqqrMmTMns2bNysaNGzNq1KjMnTs3SbY7BgAAXa1TATxjxozMmDHjdY8/9thj29zmkEMOSVNT0xseAwCAruROcAAAFEUAAwBQFAEMAEBRBDAAAEURwAAAFEUAAwBQFAEMAEBRBDAAAEURwAAAFEUAAwBQFAEMAEBRBDAAAEURwAAAFEUAAwBQFAEMAEBRBDAAAEURwAAAFEUAAwBQFAEMAEBRBDAAAEURwAAAFEUAAwBQFAEMAEBRBDAAAEURwAAAFEUAAwBQFAEMAEBRenf3AgC6yqDB/dOvb+Xe5jZsfDVrXl5fsfkA6B4CGNhj9evbO5POnl+x+Zoum5I1FZsNgO7iFAgAAIoigAEAKIoABgCgKAIYAICiCGAAAIoigAEAKIoABgCgKK4DDADAFip9I6FNm1srNlclCGAAALbQFTcS6kmcAgEAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARXErZLaq0vcABwDoKXZYOI2NjVm4cGGee+65NDU15d3vfneS5Kmnnsr06dOzevXq1NbWprGxMWPGjNmpMXqOPf0e4ABAuXZ4CsT48eNz4403ZtSoUVs8PmvWrEybNi0LFy7MtGnTMnPmzJ0eAwCArrbDAK6vr09dXd0WjzU3N2fp0qWZOHFikmTixIlZunRpWlpa3vQYAADsCm/qJM/ly5dnxIgRqa6uTpJUV1dn+PDhWb58edrb29/U2JAhQ97QGoYO3avTzx02bNAbmhvoPp35fu3O72nvJ7uOr3U57Osy9KT9vNv+llNz8ytpa2vf4fOGDRuUF19cswtWtGfpSX9IKcuOvl/fyPd0V/w59n6ya3jvLod93TPtCe+fVVW9tnnA9E0FcF1dXVasWJHW1tZUV1entbU1K1euTF1dXdrb29/UGAAA7Apv6jrAQ4cOzbhx47JgwYIkyYIFCzJu3LgMGTLkTY8BAMCusMMjwBdffHHuvPPOvPTSSznppJNSW1ub2267LbNnz8706dNzzTXXZPDgwWlsbOzY5s2OAQBAV9thAM+YMSMzZsx43eP77bdfbrnllq1u82bHAACgq7kVMgAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAUZbe9FTLArrZpc2vFbg+6YeOrWfPy+orMBcAbI4ABOqmmT3UmnT2/InM1XTYlayoyEwBvlFMgAAAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoijvBAT1GZ281XKnbEQNQJgEM9BiVvNVw8trthgHgDzkFAgCAoghgAACKIoABACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoux0AP/4xz/OsccemylTpmTy5Mm58847kyRPPfVUjj/++EyYMCHHH398nn766Y5ttjcGAABdaacCuL29Pf/wD/+QOXPmZP78+ZkzZ07OO++8tLW1ZdasWZk2bVoWLlyYadOmZebMmR3bbW8MAAC60k4fAa6qqsqaNWuSJGvWrMnw4cOzatWqLF26NBMnTkySTJw4MUuXLk1LS0uam5u3OQYAAF2t985s3KtXr1x55ZU544wzMmDAgKxduzZf+cpXsnz58owYMSLV1dVJkurq6gwfPjzLly9Pe3v7NseGDBnS6dceOnSvTj932LBBb+wTA9gFvDdtn69POezrMvSk/bxTAfzqq6/muuuuyzXXXJNDDz00999/fz7/+c9nzpw5lVrfNjU3v5K2tvYdPm/YsEF58cU1Xb6ePU1P+kMKeyrvTdvmvbsc9nXP1BUdsKv3c1VVr20eMN2pAH700UezcuXKHHrooUmSQw89NP3790/fvn2zYsWKtLa2prq6Oq2trVm5cmXq6urS3t6+zTEAAOhqO3UO8D777JMXXnghTz75ZJJk2bJlaW5uztvf/vaMGzcuCxYsSJIsWLAg48aNy5AhQzJ06NBtjgEAQFfbqSPAw4YNy+zZs3PWWWelV69eSZJLLrkktbW1mT17dqZPn55rrrkmgwcPTmNjY8d22xsDAICutFMBnCSTJ0/O5MmTX/f4fvvtl1tuuWWr22xvDAAAupI7wQEAUBQBDABAUQQwAABFEcAAABRFAAMAUBQBDABAUQQwAABFEcAAABRlp2+EQc8waHD/9Otrd8LuYtPm1gwbNqhi823Y+GrWvLy+YvMB7MkU0x6iX9/emXT2/IrN13TZlIrNBbxeTZ/qin/PrqnYbAB7NqdAAABQFAEMAEBRBDAAAEURwAAAFMUvwQHsAVxVAqDzBDDAHsBVJQA6zykQAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFCU3t29AAB6nk2bWzNs2KCKzbdh46tZ8/L6is0HsDMEMACvU9OnOpPOnl+x+Zoum5I1FZsNYOfs9CkQGzduzKxZs/KRj3wkkyZNyj//8z8nSZ566qkcf/zxmTBhQo4//vg8/fTTHdtsbwwAALrSTgfw3Llz07dv3yxcuDBNTU0566yzkiSzZs3KtGnTsnDhwkybNi0zZ87s2GZ7YwAA0JV2KoDXrl2b733veznrrLPSq1evJMlb3/rWNDc3Z+nSpZk4cWKSZOLEiVm6dGlaWlq2OwYAAF1tp84BfvbZZ1NbW5urr7469957bwYOHJizzjor/fr1y4gRI1JdXZ0kqa6uzvDhw7N8+fK0t7dvc2zIkCGdfu2hQ/fq9HMr+YscALw5b/S92Ht3OezrMvSk/bxTAdza2ppnn302f/RHf5Tzzjsvv/zlL/PpT386V111VaXWt03Nza+kra19h88bNmxQXnxxz//Vi570hwpga97Ie3Ep793Y1z1VV3TFrt7PVVW9tnnAdKcCuK6uLr179+44neGggw7K3nvvnX79+mXFihVpbW1NdXV1Wltbs3LlytTV1aW9vX2bYwAA0NV26hzgIUOG5LDDDss999yT5LWrOzQ3N2fMmDEZN25cFixYkCRZsGBBxo0blyFDhmTo0KHbHAMAgK6209cBvuCCC3L++eensbExvXv3zpw5czJ48ODMnj0706dPzzXXXJPBgwensbGxY5vtjQEAQFfa6QDed9998x//8R+ve3y//fbLLbfcstVttjcGAABdaaevAwwAALsTAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAURQADAFAUAQwAQFEEMAAARRHAAAAUpXd3LwAA9iSDBvdPv76V+/G6YeOrWfPy+orNBwhgAKiofn17Z9LZ8ys2X9NlU7KmYrMBiVMgAAAojAAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKK4DjAAXW7T5tYMGzboDW2zree7MQSwswQwAF2upk91xW4OUekbQ1T6zm1Az+c7HoCidcWd24CerWLnAF999dUZO3ZsHn/88STJQw89lMmTJ2fChAk5+eST09zc3PHc7Y0BAEBXqkgAP/LII3nooYcyatSoJElbW1vOPffczJw5MwsXLkx9fX0uvfTSHY4BAEBX2+kA3rRpUy688MLMnj2747ElS5akb9++qa+vT5JMnTo1P/jBD3Y4BgAAXW2nzwG+6qqrMnny5IwePbrjseXLl2fkyJEdHw8ZMiRtbW1ZvXr1dsdqa2s7/bpDh+7V6ee+0d88BqBnK+19fU//fPf0z4/X9KT9vFMB/OCDD2bJkiU555xzKrWeTmtufiVtbe07fN6wYYPy4ouV/H3hnqkn/aEC6GqVfF/fHd4/9+SfY6X8nN7ddMX3xa7ez1VVvbZ5wHSnAvi+++7LsmXLMn78+CTJCy+8kFNOOSUnnHBCnn/++Y7ntbS0pKqqKrW1tamrq9vmGAAAdLWdOgf49NNPz913351FixZl0aJF2WefffLVr341p556ajZs2JDFixcnSW6++eYcddRRSZIDDjhgm2MAANDVuuQ6wFVVVZkzZ05mzZqVjRs3ZtSoUZk7d+4OxwAAoKtVNIAXLVrU8f+HHHJImpqatvq87Y0BAEBXqtiNMAAAYHcggAEAKIoABgCgKAIYAICidMlVIACgq2za3Lpb3LwC6LkEMAC7lZo+1Zl09vyKzdd02ZSKzQXsHpwCAQBAUQQwAABFEcAAABRFAAMAUBQBDABAUQQwAABFEcAAABRFAAMAUBQBDABAUQQwAABFEcAAABRFAAMAUBQBDABAUQQwAABF6d3dCwAAtm3T5tYMGzaoYvNt2Phq1ry8vmLzwe5IAANAD1bTpzqTzp5fsfmaLpuSNRWbDXZPToEAAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoSu/uXgAAsOts2tyaYcMGVWSuDRtfzZqX11dkLtiVBDAAFKSmT3UmnT2/InM1XTYlayoyE+xaToEAAKAoAhgAgKIIYAAAiiKAAQAoyk4F8KpVq3LaaadlwoQJmTRpUs4888y0tLQkSR566KFMnjw5EyZMyMknn5zm5uaO7bY3BgAAXWmnArhXr1459dRTs3DhwjQ1NWXffffNpZdemra2tpx77rmZOXNmFi5cmPr6+lx66aVJst0xAGD38btLqu3Mf0k6/n/Q4P7d/BlRip26DFptbW0OO+ywjo8PPvjg3HTTTVmyZEn69u2b+vr6JMnUqVMzfvz4fPGLX9zuGACw+6jkJdUSl1Vj16nYdYDb2tpy0003paGhIcuXL8/IkSM7xoYMGZK2trasXr16u2O1tbWdfr2hQ/fq9HMrdcFvAKBr+Zm95+pJ+7ZiAXzRRRdlwIAB+eQnP5m77rqrUtNuU3PzK2lra9/h84YNG5QXX9zz/z7Zk/5QAcCbVcLP7N1BV3TFrt63VVW9tnnAtCIB3NjYmGeeeSbXXnttqqqqUldXl+eff75jvKWlJVVVVamtrd3uGAAAdLWdvgza5ZdfniVLlmTevHmpqalJkhxwwAHZsGFDFi9enCS5+eabc9RRR+1wDAAAutpOHQF+4oknct1112XMmDGZOnVqkmT06NGZN29e5syZk1mzZmXjxo0ZNWpU5s6dmySpqqra5hgAAHS1nQrg/fffP4899thWxw455JA0NTW94TEAAOhK7gQHAEBRBDAAAEWp2GXQAAB2xu/uLFcpGza+mjUvr6/YfOw5BDAA0CO4sxy7ilMgAAAoiiPAAACdMGhw//TrW7l0copG9xHAAACd0K9vb6do7CGcAgEAQFEEMAAARRHAAAAUxTnAAMAeqdLXFWbPIYABgD1SV1xXmD2DUyAAACiKAAYAoCgCGACAoghgAACKIoABACiKAAYAoCgCGACAoghgAACK4kYY3WTQ4P7p19eXHwBgV1Ng3aRf397uTgMA0A2cAgEAQFEcAQYA6AabNrdm2LBBFZlrw8ZXs+bl9RWZqwQCGACgG9T0qa7Y6ZBNl03JmorMVAanQAAAUBRHgAEAdnOVPJ2iBAIYAGA3V8nTKZI9/+pSToEAAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAoAhgAgKIIYAAAitK7uxewuxg0uH/69fXlAgDY3XVb0T311FOZPn16Vq9endra2jQ2NmbMmDHdtZwd6te3dyadPb9i8zVdNqVicwEA0HnddgrErFmzMm3atCxcuDDTpk3LzJkzu2spAAAUpFuOADc3N2fp0qX52te+liSZOHFiLrroorS0tGTIkCGdmqOqqlenX++NPHd7hu/dvyLzmK9nzVXafD15baXN15PX1tPn68lrK22+nry20ubryWtLKtdjlXi9Xu3t7e27cC1JkiVLluS8887Lbbfd1vHYMccck7lz5+a9733vrl4OAAAFcRUIAACK0i0BXFdXlxUrVqS1tTVJ0trampUrV6aurq47lgMAQEG6JYCHDh2acePGZcGCBUmSBQsWZNy4cZ0+/xcAAN6sbjkHOEmWLVuW6dOn5+WXX87gwYPT2NiYd77znd2xFAAACtJtAQwAAN3BL8EBAFAUAQwAQFEEMAAARRHAAAAUZY8N4KeeeirHH398JkyYkOOPPz5PP/10dy+JLtDY2JiGhoaMHTs2jz/+eHcvhy6yatWqnHbaaZkwYUImTZqUM888My0tLd29LLrAGWeckcmTJ+fYY4/NtGnT8uijj3b3kuhiV199tffwPVhDQ0OOOuqoTJkyJVOmTMlPf/rT7l5Skj34KhAnnnhi/vIv/zJTpkzJ/Pnz853vfCc33HBDdy+LClu8eHFGjRqVT3ziE7n22mvz7ne/u7uXRBdYvXp1HnvssRx22GFJXvuLz//93//lkksu6eaVUWlr1qzJoEGDkiQ//OEPM2/evHz3u9/t5lXRVR555JFcccUVefLJJ72H76EaGhp65L7dI48ANzc3Z+nSpZk4cWKSZOLEiVm6dKkjRnug+vp6dxAsQG1tbUf8JsnBBx+c559/vhtXRFf5XfwmySuvvJJevXp142roSps2bcqFF16Y2bNnd/dSKFDv7l5AV1i+fHlGjBiR6urqJEl1dXWGDx+e5cuXu9sc7Oba2tpy0003paGhobuXQhf5p3/6p9xzzz1pb2/P9ddf393LoYtcddVVmTx5ckaPHt3dS6GLnXPOOWlvb8+hhx6av//7v8/gwYO7e0l75hFgYM910UUXZcCAAfnkJz/Z3Uuhi3zhC1/IT37yk/zd3/1d5syZ093LoQs8+OCDWbJkSaZNm9bdS6GL3Xjjjfn+97+f73znO2lvb8+FF17Y3UtKsocGcF1dXVasWJHW1tYkSWtra1auXOmfymE319jYmGeeeSZXXnllqqr2yLcvfs+xxx6be++9N6tWrerupVBh9913X5YtW5bx48enoaEhL7zwQk455ZTcfffd3b00Kux37VVTU5Np06blgQce6OYVvWaP/AkydOjQjBs3LgsWLEiSLFiwIOPGjXP6A+zGLr/88ixZsiTz5s1LTU1Ndy+HLrB27dosX7684+NFixblLW95S2pra7tvUXSJ008/PXfffXcWLVqURYsWZZ999slXv/rVfOADH+jupVFB69aty5o1a5Ik7e3tuf322zNu3LhuXtVr9tirQCxbtizTp0/Pyy+/nMGDB6exsTHvfOc7u3tZVNjFF1+cO++8My+99FL23nvv1NbW5rbbbuvuZVFhTzzxRCZOnJgxY8akX79+SZLRo0dn3rx53bwyKumll17KGWeckfXr16eqqipvectbct555+W9731vdy+NLtZTrxTAznn22Wfz2c9+Nq2trWlra8t+++2XGTNmZPjw4d29tD03gAEAYGv2yFMgAABgWwQwAABFEcAAABRFAAMAUBQBDABAUQQwwC526qmn5rvf/e4uea3FixdnwoQJOz3P9OnTK7AagJ5BAAM9VkNDQ372s59t8ditt96aj3/84920osq4/vrr89GPfnSXvFZ9fX0WLlzYJXPffvvtmTp1ag466KCccMIJrxt/9NFHc9xxx+Wggw7Kcccdl0cffbRjrL29PXPnzs1hhx2Www47LHPnzs32rsrZ1NSUI488MgcffHDOOOOMrF69umNs9erV+cxnPpODDz44Rx55ZJqamjq9LVAmAQywi7S3t6etra27l9FpbW1tufjii/PBD34wCxYsSENDQ775zW92jNfW1ubEE0/Maaed9rptN23alDPOOCOTJ0/Offfdl2OPPTZnnHFGNm3alCT55je/mR/+8IeZP39+vv/97+fHP/5xbr755q2u44knnsjMmTMzZ86c3HPPPenfv38uuOCCjvELL7wwffr0yT333JO5c+dm9uzZeeKJJzq1LVAmAQzs1pYtW5YTTjgh9fX1+Yu/+Iv86Ec/6hg74YQTcsstt3R8/PtHj9vb23PJJZfk8MMPzyGHHJJJkybl8ccfT/JavDU2NubP//zPc8QRR2TmzJnZsGHDVl//1ltvzdSpU3PhhRfm0EMPzVFHHZWf//znW6zhiiuu6DhS+uyzz75uXd/61rdy9NFH533ve1+OOeaYPPLII0mSFStW5LOf/Wze//73p6GhITfccEPHNg8//HCOO+64HHLIITniiCPyxS9+cavru/fee/PBD36w4+OGhoZ89atfzaRJk3LooYfm85//fDZu3LjVbW+//fb813/9V2655ZZMnDgx3/jGNzJ27NiO8SOOOCLHHHNMRowY8bpt/+d//ievvvpqPvWpT6WmpiYnnnhi2tvb84tf/CJJ8r3vfS8nn3xy9tlnn4wYMSInnXTSNk8LaWpqSkNDQ/74j/84AwcOzFlnnZW77rorr7zyStatW5c777wzZ511VgYOHJj6+vo0NDRk/vz5O9wWKJcABnZbmzdvzqc//en86Z/+aX72s59lxowZOeecc/Lkk0/ucNu77747ixcvzsKFC3P//ffnyiuvTG1tbZLk0ksvzVNPPZXvfe97ufPOO7Ny5crt3nb54Ycfztve9rb84he/yOc+97mceeaZW/wz+/z583PRRRflgQceyMiRI7fY9o477si//Mu/pLGxMQ888ED+9V//NbW1tWlra8vf/u3fZuzYsfnv//7vfP3rX8/Xv/71/PSnP02SfOELX8iJJ56YBx54IHfddVeOPvroTn/d7rjjjlx//fX50Y9+lMceeyy33nrrVp/30ksvZfTo0R2Bu88+++Tggw/u1Gv85je/ydixY9OrV6+Ox8aOHZvf/OY3SV47Mvue97ynY+w973lPx1HbP/TEE09sEd5ve9vb0qdPnzz99NN5+umnU11dnXe84x1bzPX7r7OtbYFyCWCgR/vMZz6T+vr6jv9+/5+vf/nLX2bdunU5/fTTU1NTk8MPPzxHHnlkbrvtth3O27t376xduzZPPvlk2tvbs99++2X48OFpb2/Pt771rZx//vmpra3NXnvtlb/5m7/Z7pxDhgzJpz71qfTp0yfHHHNM3vGOd+QnP/lJx/hHP/rR7L///undu3f69Omzxbbf/va3c+qpp+bAAw9Mr1698va3vz2jRo3Kr371q7S0tOTMM89MTU1N9t1333zsYx/L7bff3rH+3/72t2lpacnAgQM7HabJa0elR4wYkdra2hx55JFbnJv7+44++ug888wzmTZtWp544omOo7edsXbt2gwaNGiLx/baa6+sXbs2SbJu3brstddeHWODBg3KunXrtnoe8Lp167Y51x/O87u5fv91trcOoEy9u3sBANszb968HHHEER0f33rrrR2nD6xcuTL77LNPqqr+/9/lR44cmRUrVuxw3sMPPzyf+MQncuGFF+a5557LRz7ykZx33nnZuHFj1q9fn+OOO67juTs6d3fEiBFbHOkcOXJkVq5c2fFxXV3dNrddvnx53va2t73u8eeeey4rV65MfX19x2Otra0dH3/hC1/Il7/85Rx99NEZPXp0zjzzzBx55JE7/LyTZNiwYR3/379//y3W+oef1x133JFf/OIXueSSS3L++efnoIMOyhVXXLHD1xg4cODrTjNYu3ZtBg4cmCQZMGDAFhH6yiuvZMCAAVt8HX9nwIABr5vrlVdeycCBA1NVVbXNsR1tC5TLEWBgtzV8+PC88MILW8Tp8uXLO/7Jvn///lm/fn3H2EsvvbTF9ieeeGJuvfXW3H777Xn66adz/fXXZ++9906/fv1y2223ZfHixVm8eHHuv//+PPjgg9tcx4oVK7Y4crl8+fIMHz684+OtRd3v1NXV5be//e1WHx89enTHGhYvXpwHH3ww//Zv/5YkGTNmTC6//PL8/Oc/z2mnnZbPfe5zWbdu3TZf583q27dvPvShD+V973tfbr755tx+++1paWnZ4Xbvete78thjj23xdXnsscfyrne9K0my//7759e//nXH2K9//evsv//+W53rD5/77LPPZvPmzRkzZkzGjBmT1tbWLU5p+PWvf73N1/n9bYFyCWBgt3XggQemX79+uf7667N58+bce++9WbRoUY455pgkybhx43LXXXdl/fr1eeaZZ/Ltb3+7Y9uHH344v/zlL7N58+b0798/NTU1qaqqSlVVVf76r/86l1xySZqbm5O8Fri/O/d2a1paWnLDDTdk8+bNueOOO7Js2bJ86EMf6tTn8Fd/9Vf593//9yxZsiTt7e155pln8txzz+XAAw/MwIED85WvfCUbNmxIa2trHn/88Tz88MNJXjuvuKWlJVVVVRk8eHCSbHEkvBJ+/vOfd5xLmySPP/54+vfv33HKQWtrazZu3JhXX301bW1t2bhxYzZv3pwk+ZM/+ZNUV1fnhhtuyKZNm/Kf//mfSZL3v//9SZIpU6bka1/7WlasWJEVK1bka1/72haXhmtoaOg4N3nSpEn58Y9/nMWLF2fdunW56qqr8uEPfzh77bVXBgwYkA9/+MP58pe/nHXr1uX+++/Pj370o0yZMmWH2wLlcgoEsNuqqanJtddemwsuuCDXXXddRowYkTlz5mS//fZLknzqU5/Kr371qxxxxBEZO3ZsJk2a1HFd4bVr1+aSSy7J//7v/6ampiYf+MAHcsoppyRJzj333MybNy8f+9jHsmrVqowYMSIf//jH82d/9mdbXceBBx6YZ555Ju9///vz1re+NV/+8pez9957d+pzOProo7N69eqcffbZWblyZUaNGpU5c+Zk1KhRufbaa9PY2Jjx48dn06ZNecc73pHPf/7zSZKf/vSn+dKXvpQNGzZk5MiRueKKK9KvX7+d/Ipuqa2tLeeff35WrlyZVatW5YEHHsicOXNSU1OT5LUI/8d//Mctvg4f/ehH86UvfSk1NTWZN29eZsyYkcsuuyz77bdf5s2b17Ht1KlT8+yzz2bSpElJXvuLwNSpU5O8dhWOVatW5aCDDkry2lHcCy64IOecc05Wr16dww8/fIurXsyaNSvnn39+jjjiiNTW1mb27NkdR5N3tC1Qpl7t27vyOADb9btzkm+66abuXkqXmj59er70pS/tktdavHhxvvGNb+Tyyy/fJa8HlMcRYAB6lN9d8QOgqzgHGIAd2lVHfwF2BadAAABQFEeAAQAoigAGAKAoAhgAgKIIYAAAiiKAAQAoigAGAKAo/w+2Yqq1EfEaVQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 842.4x595.44 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Plot the distribution of the target variable using a histogram\n",
    "sns.set(rc={'figure.figsize':(11.7,8.27)})\n",
    "plt.hist(cali_df['PRICE'], bins=30)\n",
    "plt.xlabel(\"House prices in $100,000\")\n",
    "plt.show()\n",
    "\n",
    "# We can see from the plot that the values of PRICE are distributed normally with few outliers. \n",
    "# Most of the house are around 100,000-200,000 range (in $100,000 scale)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40fe078c-4fc4-46f8-b52a-4cdb6af552e8",
   "metadata": {},
   "source": [
    "## Step 2 Train the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "61949bfd-4444-456d-8e9d-1fa6e6c1cdd8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(16512, 8)\n",
      "(4128, 8)\n",
      "(16512,)\n",
      "(4128,)\n"
     ]
    }
   ],
   "source": [
    "# Split target variable and feature variables\n",
    "X = cali_df.drop('PRICE', axis = 1)\n",
    "y = cali_df['PRICE']\n",
    "\n",
    "# Splitting the data into training and testing sets in numpy arrays\n",
    "# We train the model with 80% of the samples and test with the remaining 20%\n",
    "# use train_test_split function provided by scikit-learn library\n",
    "X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, random_state=42)\n",
    "\n",
    "print(X_train.shape)\n",
    "print(X_test.shape)\n",
    "print(y_train.shape)\n",
    "print(y_test.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "2a3cef80-d793-4c28-990f-1464d800ae8c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "LinearRegression()"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Use scikit-learn’s LinearRegression to train the model on both the training and evaluate it on the test sets\n",
    "\n",
    "# Create a Linear regressor using all the feature variables\n",
    "reg_all = LinearRegression()\n",
    "\n",
    "# Train the model using the training sets\n",
    "reg_all.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "825e7c0d-0cbc-4b43-a679-4216facc06fe",
   "metadata": {},
   "source": [
    "# Step 3 Evaluate the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "22c6856a-ebdf-4ab4-a10c-91153a94d95f",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Actual</th>\n",
       "      <th>Predicted</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>20046</th>\n",
       "      <td>0.47700</td>\n",
       "      <td>0.719123</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3024</th>\n",
       "      <td>0.45800</td>\n",
       "      <td>1.764017</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15663</th>\n",
       "      <td>5.00001</td>\n",
       "      <td>2.709659</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>20484</th>\n",
       "      <td>2.18600</td>\n",
       "      <td>2.838926</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9814</th>\n",
       "      <td>2.78000</td>\n",
       "      <td>2.604657</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15362</th>\n",
       "      <td>2.63300</td>\n",
       "      <td>1.991746</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16623</th>\n",
       "      <td>2.66800</td>\n",
       "      <td>2.249839</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>18086</th>\n",
       "      <td>5.00001</td>\n",
       "      <td>4.468770</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2144</th>\n",
       "      <td>0.72300</td>\n",
       "      <td>1.187511</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3665</th>\n",
       "      <td>1.51500</td>\n",
       "      <td>2.009403</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>4128 rows × 2 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "        Actual  Predicted\n",
       "20046  0.47700   0.719123\n",
       "3024   0.45800   1.764017\n",
       "15663  5.00001   2.709659\n",
       "20484  2.18600   2.838926\n",
       "9814   2.78000   2.604657\n",
       "...        ...        ...\n",
       "15362  2.63300   1.991746\n",
       "16623  2.66800   2.249839\n",
       "18086  5.00001   4.468770\n",
       "2144   0.72300   1.187511\n",
       "3665   1.51500   2.009403\n",
       "\n",
       "[4128 rows x 2 columns]"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Check the model performance on the train dataset \n",
    "y_pred = reg_all.predict(X_test)\n",
    "\n",
    "pred_df = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})\n",
    "\n",
    "pred_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "2d57f157-c654-42cd-ad1a-1312d61ae91b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "R^2: 0.58\n"
     ]
    }
   ],
   "source": [
    "# Determine accuracy uisng 𝑅^2\n",
    "# 𝑅^2 : R squared is another way to evaluate the performance of a regression model.\n",
    "# The difference between the samples in the dataset and the predictions made by the model.\n",
    "# 1, means that the model is perfect and if its value is 0, it means that the model will perform poorly.\n",
    "r2 = round(reg_all.score(X_test, y_test),2)\n",
    "\n",
    "print(\"R^2: {}\".format(r2))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "default:Python",
   "language": "python",
   "name": "conda-env-default-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
